// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem.alt.s3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.data.SleepLoopTime;
import org.refcodes.exception.Trap;
import org.refcodes.filesystem.ConcurrentAccessException;
import org.refcodes.filesystem.FileAlreadyExistsException;
import org.refcodes.filesystem.FileHandle;
import org.refcodes.filesystem.FileHandle.MutableFileHandle;
import org.refcodes.filesystem.FileHandleImpl;
import org.refcodes.filesystem.FileSystem;
import org.refcodes.filesystem.FileSystemUtility;
import org.refcodes.filesystem.IllegalFileException;
import org.refcodes.filesystem.IllegalKeyException;
import org.refcodes.filesystem.IllegalNameException;
import org.refcodes.filesystem.IllegalPathException;
import org.refcodes.filesystem.NoCreateAccessException;
import org.refcodes.filesystem.NoDeleteAccessException;
import org.refcodes.filesystem.NoListAccessException;
import org.refcodes.filesystem.NoReadAccessException;
import org.refcodes.filesystem.NoWriteAccessException;
import org.refcodes.filesystem.UnknownFileException;
import org.refcodes.filesystem.UnknownFileSystemException;
import org.refcodes.filesystem.UnknownKeyException;
import org.refcodes.filesystem.UnknownPathException;
import org.refcodes.time.DateFormat;

import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.exception.SdkServiceException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.model.CommonPrefix;
import software.amazon.awssdk.services.s3.model.CopyObjectRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Object;

/**
 * A {@link FileSystem} implementation for S3.
 */
public class S3FileSystem extends AbstractS3Client implements FileSystem {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( S3FileSystem.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String METADATA_CREATED_DATE = "CreatedDate";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final HashSet<Thread> _threadCache = new HashSet<>();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new s 3 file system impl.
	 *
	 * @param aBucketName the bucket name
	 * @param aAccessKey the access key
	 * @param aSecretKey the secret key
	 */
	public S3FileSystem( String aBucketName, String aAccessKey, String aSecretKey ) {
		super( aBucketName, aAccessKey, aSecretKey );
	}

	/**
	 * Instantiates a new s 3 file system impl.
	 *
	 * @param aBucketName the bucket name
	 * @param aAccessKey the access key
	 * @param aSecretKey the secret key
	 * @param aEndPoint the end point
	 */
	public S3FileSystem( String aBucketName, String aAccessKey, String aSecretKey, String aEndPoint ) {
		super( aBucketName, aAccessKey, aSecretKey, aEndPoint );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Checks for file.
	 *
	 * @param aKey the key
	 * 
	 * @return true, if successful
	 * 
	 * @throws IllegalKeyException the illegal key exception
	 * @throws NoListAccessException the no list access exception
	 * @throws UnknownFileSystemException the unknown file system exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public boolean hasFile( String aKey ) throws IllegalKeyException, NoListAccessException, UnknownFileSystemException, IOException {
		if ( aKey == null || aKey.isEmpty() ) {
			throw new IllegalKeyException( "The provided key is invalid.", aKey );
		}
		try {
			final HeadObjectRequest theHeadObjectRequest = HeadObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aKey ).build();
			getAmazonS3Client().headObject( theHeadObjectRequest );
		}
		catch ( SdkServiceException e ) {
			// LOGGER.log( Level.WARNING, "Cannot test key \"" + aKey + "\" as of: " + Trap.toMessage( e ), e );
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasFile( String aPath, String aName ) throws IllegalPathException, IllegalNameException, NoListAccessException, UnknownFileSystemException, IOException {
		if ( aPath == null || aPath.isEmpty() ) {
			throw new IllegalPathException( "The provided path is invalid.", aPath );
		}
		if ( aName == null || aName.isEmpty() ) {
			throw new IllegalNameException( aName, "The provided name is invalid." );
		}
		boolean result = false;
		try {
			result = hasFile( FileSystemUtility.toKey( aPath, aName ) );
		}
		catch ( IllegalKeyException e ) {
			throw new IOException( "The provided key <" + FileSystemUtility.toKey( aPath, aName ) + "> is not valid for the S3 filesystem!", e );
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasFile( FileHandle aFileHandle ) throws NoListAccessException, UnknownFileSystemException, IOException, IllegalFileException {
		boolean result = false;
		try {
			result = hasFile( aFileHandle.toKey() );
		}
		catch ( IllegalKeyException e ) {
			throw new IOException( "The provided key <" + aFileHandle.toKey() + "> is not valid for the S3 filesystem!", e );
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle createFile( String aKey ) throws FileAlreadyExistsException, NoCreateAccessException, IllegalKeyException, UnknownFileSystemException, IOException, NoListAccessException {
		if ( aKey == null || aKey.isEmpty() ) {
			throw new IllegalKeyException( "The provided key is invalid.", aKey );
		}
		if ( hasFile( aKey ) ) {
			throw new FileAlreadyExistsException( aKey, "A file handle with the given key \"" + aKey + "\" already exists." );
		}
		final String path = FileSystemUtility.getPath( aKey );
		final String name = FileSystemUtility.getName( aKey );
		final FileHandle fileHandle = new FileHandleImpl( path, name, 0L, new Date(), new Date() );
		return fileHandle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle createFile( String aPath, String aName ) throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, IOException, NoListAccessException {
		if ( aPath == null || aPath.isEmpty() ) {
			throw new IllegalPathException( "The provided path is invalid.", aPath );
		}
		if ( aName == null || aName.isEmpty() ) {
			throw new IllegalNameException( aName, "The provided name is invalid." );
		}
		final FileHandle fileHandle = new FileHandleImpl( aPath, aName, 0L, new Date(), new Date() );
		if ( hasFile( aPath, aName ) ) {
			throw new FileAlreadyExistsException( fileHandle.toKey(), "A file handle with the given path \"" + aPath + "\" and name \"" + aName + "\" (key = \"" + FileSystemUtility.toKey( aPath, aName ) + "\") already exists." );
		}
		return fileHandle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle getFileHandle( String aKey ) throws NoListAccessException, IllegalKeyException, UnknownFileSystemException, IOException, UnknownKeyException {
		if ( aKey == null || aKey.isEmpty() ) {
			throw new IllegalKeyException( "The provided key is invalid.", aKey );
		}
		if ( !hasFile( aKey ) ) {
			throw new UnknownKeyException( "A file with the given key \"" + aKey + "\"does not exist!", aKey );
		}
		final HeadObjectRequest theHeadObjectRequest = HeadObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aKey ).build();
		final HeadObjectResponse theHeadObjectResponse = getAmazonS3Client().headObject( theHeadObjectRequest );
		final Date createdDate = getCreatedDate( theHeadObjectResponse.metadata() );
		final String path = FileSystemUtility.getPath( aKey );
		final String name = FileSystemUtility.getName( aKey );
		final FileHandle fileHandle = new FileHandleImpl( path, name, theHeadObjectResponse.contentLength(), createdDate, Date.from( theHeadObjectResponse.lastModified() ) );
		return fileHandle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle getFileHandle( String aPath, String aName ) throws NoListAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, IOException, UnknownKeyException {
		if ( aPath == null || aPath.isEmpty() ) {
			throw new IllegalPathException( "The provided path is invalid.", aPath );
		}
		if ( aName == null || aName.isEmpty() ) {
			throw new IllegalNameException( aName, "The provided name is invalid." );
		}
		if ( !hasFile( aPath, aName ) ) {
			throw new UnknownKeyException( "A filke with the given path \"" + aPath + "\" and name \"" + aName + "\" (= key \"" + FileSystemUtility.toKey( aPath, aName ) + "\") does not exists.", FileSystemUtility.toKey( aPath, aName ) );
		}
		final String key = FileSystemUtility.toKey( aPath, aName );
		final HeadObjectRequest theHeadObjectRequest = HeadObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( key ).build();
		final HeadObjectResponse theHeadObjectResponse = getAmazonS3Client().headObject( theHeadObjectRequest );
		final Date createdDate = getCreatedDate( theHeadObjectResponse.metadata() );
		final FileHandle fileHandle = new FileHandleImpl( aPath, aName, theHeadObjectResponse.contentLength(), createdDate, Date.from( theHeadObjectResponse.lastModified() ) );
		return fileHandle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromFile( FileHandle aFromFileHandle, OutputStream aOutputStream ) throws ConcurrentAccessException, UnknownFileException, NoReadAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		if ( !hasFile( aFromFileHandle ) ) {
			throw new UnknownFileException( aFromFileHandle, "The given \"from\" file handle was not found." );
		}
		final GetObjectRequest theGetObjectRequest = GetObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aFromFileHandle.toKey() ).build();
		final ResponseInputStream<GetObjectResponse> s3Object = getAmazonS3Client().getObject( theGetObjectRequest );
		if ( s3Object == null ) {
			throw new UnknownFileException( aFromFileHandle, "The given \"from\" file handle was not found." );
		}
		try {
			s3Object.transferTo( aOutputStream );
		}
		finally {
			try {
				s3Object.close();
			}
			catch ( Exception eignore ) {
				// LOGGER.log( Level.WARNING, "Unable to close the output stream for bucket <{0}> and key \"{1}\" as of: " + Trap.toMessage( ignore ), ignore, getAmazonS3BucketName(), aToFileHandle.toKey() );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void toFile( FileHandle aToFileHandle, InputStream aInputStream ) throws ConcurrentAccessException, UnknownFileException, NoWriteAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		try {
			final Map<String, String> metadata = new HashMap<>();
			if ( aToFileHandle.getCreatedDate() != null ) {
				final String createdDateString = DateFormat.NORM_DATE_FORMAT.getFormatter().format( Instant.ofEpochMilli( aToFileHandle.getCreatedDate().getTime() ) );
				metadata.put( METADATA_CREATED_DATE, createdDateString );
			}
			final PutObjectRequest thePutObjectRequest = PutObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aToFileHandle.toKey() ).metadata( metadata ).build();
			getAmazonS3Client().putObject( thePutObjectRequest, RequestBody.fromBytes( aInputStream.readAllBytes() ) );
		}
		catch ( AwsServiceException ase ) {
			throw new IOException( Trap.asMessage( ase ), ase );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromFile( FileHandle aFromFileHandle ) throws ConcurrentAccessException, UnknownFileException, NoReadAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		if ( !hasFile( aFromFileHandle ) ) {
			throw new UnknownFileException( aFromFileHandle, "The given \"from\" file handle was not found." );
		}
		final GetObjectRequest theGetObjectRequest = GetObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aFromFileHandle.toKey() ).build();
		final ResponseInputStream<GetObjectResponse> s3Object = getAmazonS3Client().getObject( theGetObjectRequest );
		if ( s3Object == null ) {
			throw new UnknownFileException( aFromFileHandle, "The given \"from\" file handle was not found." );
		}
		return s3Object;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputStream toFile( final FileHandle aToFileHandle ) throws ConcurrentAccessException, UnknownFileException, NoWriteAccessException, UnknownFileSystemException, IOException, IllegalFileException {
		final PipedInputStream thePipedInputStream = new PipedInputStream();
		final PipedOutputStream thePipedOutputStream = new PipedOutputStream( thePipedInputStream );
		// ---------------------------------------------------------------------
		// Start a new thread for the request, since the request is blocking the
		// current thread because the amazon SDK waits for the given
		// InputStream to be closed .
		// ---------------------------------------------------------------------
		final Thread t = new Thread() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void run() {
				try {
					final Map<String, String> metadata = new HashMap<>();
					if ( aToFileHandle.getCreatedDate() != null ) {
						final String createdDateString = DateFormat.NORM_DATE_FORMAT.getFormatter().format( Instant.ofEpochMilli( aToFileHandle.getCreatedDate().getTime() ) );
						metadata.put( METADATA_CREATED_DATE, createdDateString );
					}
					final PutObjectRequest thePutObjectRequest = PutObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aToFileHandle.toKey() ).metadata( metadata ).build();
					getAmazonS3Client().putObject( thePutObjectRequest, RequestBody.fromBytes( thePipedInputStream.readAllBytes() ) );
				}
				catch ( AwsServiceException | SdkClientException | IOException e ) {
					LOGGER.log( Level.SEVERE, "Unable to issue the PUT object request for bucket <" + getAmazonS3BucketName() + "> and key \"" + aToFileHandle.toKey() + "\" (closing the output stream) as of: " + Trap.asMessage( e ), e );
					try {
						thePipedOutputStream.close();
					}
					catch ( IOException ignore ) {
						// LOGGER.log( Level.WARNING, "Unable to close the output stream for bucket <{0}> and key \"{1}\" as of: " + Trap.toMessage( ignore ), ignore, getAmazonS3BucketName(), aToFileHandle.toKey() );
					}
				}
				finally {
					// Remove this thread instance from the thread cache.
					_threadCache.remove( this );
				}
			}
		};
		// Prevent the thread from dying upon a system's exit:
		t.setDaemon( true );
		// Add the thread to the thread cache.
		_threadCache.add( t );
		t.start();
		return thePipedOutputStream;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromFile( FileHandle aFileHandle, File aToFile ) throws ConcurrentAccessException, UnknownFileException, NoReadAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		if ( !hasFile( aFileHandle ) ) {
			throw new UnknownFileException( aFileHandle, "The given file handle was not found." );
		}
		final GetObjectRequest theGetObjectRequest = GetObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aFileHandle.toKey() ).build();
		final ResponseInputStream<GetObjectResponse> s3Object = getAmazonS3Client().getObject( theGetObjectRequest );
		if ( s3Object == null ) {
			throw new UnknownFileException( aFileHandle, "The given file handle was not found." );
		}
		final OutputStream theOutputStream = new FileOutputStream( aToFile );
		s3Object.transferTo( theOutputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void toFile( FileHandle aFileHandle, File aFile ) throws ConcurrentAccessException, UnknownFileException, NoWriteAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		final PutObjectRequest thePutObjectRequest = PutObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aFileHandle.toKey() ).build();
		getAmazonS3Client().putObject( thePutObjectRequest, RequestBody.fromFile( aFile ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void toFile( FileHandle aFileHandle, byte[] aBuffer ) throws ConcurrentAccessException, UnknownFileException, NoWriteAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		final Map<String, String> metadata = new HashMap<>();
		if ( aFileHandle.getCreatedDate() != null ) {
			final String createdDateString = DateFormat.NORM_DATE_FORMAT.getFormatter().format( Instant.ofEpochMilli( aFileHandle.getCreatedDate().getTime() ) );
			metadata.put( METADATA_CREATED_DATE, createdDateString );
		}
		final PutObjectRequest thePutObjectRequest = PutObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aFileHandle.toKey() ).metadata( metadata ).build();
		getAmazonS3Client().putObject( thePutObjectRequest, RequestBody.fromBytes( aBuffer ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle renameFile( FileHandle aFileHandle, String aNewName ) throws UnknownFileException, ConcurrentAccessException, FileAlreadyExistsException, NoCreateAccessException, NoDeleteAccessException, IllegalNameException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		if ( aNewName == null || aNewName.isEmpty() ) {
			throw new IllegalNameException( aNewName, "The provided new name \"" + aFileHandle + "\" is invalid." );
		}
		FileHandle fileHandle = null;
		try {
			fileHandle = moveFile( aFileHandle, FileSystemUtility.toKey( aFileHandle.getPath(), aNewName ) );
		}
		catch ( IllegalKeyException e ) {
			throw new IllegalNameException( aNewName, "The provided new name \"" + aNewName + "\" is invalid." );
		}
		return fileHandle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle moveFile( FileHandle aFileHandle, String aNewKey ) throws UnknownFileException, ConcurrentAccessException, FileAlreadyExistsException, NoCreateAccessException, NoDeleteAccessException, IllegalKeyException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		if ( aNewKey == null || aNewKey.isEmpty() ) {
			throw new IllegalKeyException( "The provided new key \"" + aNewKey + "\" is invalid.", aNewKey );
		}
		if ( !hasFile( aFileHandle ) ) {
			throw new UnknownFileException( aFileHandle, "The given file handle handle was not found." );
		}
		if ( hasFile( aNewKey ) ) {
			throw new FileAlreadyExistsException( aNewKey, "A file handle with the given new key \"" + aNewKey + "\" already exists." );
		}
		// When deprecation causes removal of "copySource(...)" |-->
		final String theSource = URLEncoder.encode( getAmazonS3BucketName() + "/" + aFileHandle.toKey(), StandardCharsets.UTF_8.name() );
		@SuppressWarnings("deprecation")
		final CopyObjectRequest theCopyObjectRequest = CopyObjectRequest.builder().copySource( theSource ).destinationBucket( getAmazonS3BucketName() ).destinationKey( aNewKey ).build();
		// CopyObjectRequest theCopyObjectRequest = CopyObjectRequest.builder().sourceBucket( getAmazonS3BucketName() ).sourceKey( aFileHandle.toKey() ).destinationBucket( getAmazonS3BucketName() ).destinationKey( aNewKey ).build();
		// When deprecation causes removal "copySource(...)" <--|
		getAmazonS3Client().copyObject( theCopyObjectRequest );
		deleteFile( aFileHandle );
		FileHandle theNewFileHandle = null;

		try {
			theNewFileHandle = getFileHandle( aNewKey );
		}
		catch ( UnknownKeyException e ) { /* ignore, handled below */}

		// -----------------------------------------------------------------
		// The file was deleted at its new key just after we moved it. We don't
		// throw an exception as the deletion of the new key could as well have
		// happened after returning from this method.
		// -----------------------------------------------------------------
		if ( theNewFileHandle == null ) {
			LOGGER.log( Level.WARNING, "The file handle which moved from \"" + aFileHandle.toKey() + "\" to  \"" + aFileHandle.getName() + "\" does not exist any more at it's target location! This should never happen (we probably have encountered a thread race condition)." );
			final MutableFileHandle theMutableNewFileHandle = aFileHandle.toMutableFileHandle();
			theMutableNewFileHandle.setPath( FileSystemUtility.getPath( aNewKey ) );
			theMutableNewFileHandle.setName( FileSystemUtility.getName( aNewKey ) );
			return theMutableNewFileHandle;
		}
		return theNewFileHandle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteFile( FileHandle aFileHandle ) throws ConcurrentAccessException, UnknownFileException, NoDeleteAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		if ( !hasFile( aFileHandle ) ) {
			throw new UnknownFileException( aFileHandle, "The given file handle was not found." );
		}
		final DeleteObjectRequest theDeleteObjectRequest = DeleteObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( aFileHandle.toKey() ).build();
		getAmazonS3Client().deleteObject( theDeleteObjectRequest );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasFiles( String aPath, boolean isRecursively ) throws NoListAccessException, IllegalPathException, UnknownFileSystemException, IOException {
		if ( aPath == null || aPath.isEmpty() ) {
			throw new IllegalPathException( "The provided path \"" + aPath + "\" is invalid.", aPath );
		}
		try {
			final List<FileHandle> fileHandles = getFileHandles( aPath, isRecursively );
			return fileHandles.isEmpty() ? false : true;
		}
		catch ( UnknownPathException e ) {
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<FileHandle> getFileHandles( String aPath, boolean isRecursively ) throws NoListAccessException, UnknownPathException, IllegalPathException, UnknownFileSystemException, IOException {
		if ( aPath == null || aPath.isEmpty() ) {
			throw new IllegalPathException( "The provided path \"" + aPath + "\" is invalid.", aPath );
		}
		if ( !aPath.endsWith( "" + FileSystem.PATH_DELIMITER ) ) {
			aPath += FileSystem.PATH_DELIMITER;
		}
		final List<FileHandle> eResult = new ArrayList<>();
		ListObjectsV2Request eListObjectsRequest = ListObjectsV2Request.builder().bucket( getAmazonS3BucketName() ).delimiter( "" + FileSystem.PATH_DELIMITER ).prefix( aPath ).build();
		ListObjectsV2Response eObjectListing = getAmazonS3Client().listObjectsV2( eListObjectsRequest );
		HeadObjectRequest eHeadObjectRequest;
		HeadObjectResponse eHeadObjectResponse;
		String eNextToken;
		while ( eObjectListing.contents() != null && !eObjectListing.contents().isEmpty() ) {
			final List<S3Object> batchObjects = eObjectListing.contents();
			for ( S3Object summary : batchObjects ) {
				if ( !aPath.equals( summary.key() ) ) {
					eHeadObjectRequest = HeadObjectRequest.builder().bucket( getAmazonS3BucketName() ).key( summary.key() ).build();
					eHeadObjectResponse = getAmazonS3Client().headObject( eHeadObjectRequest );
					final Date createdDate = getCreatedDate( eHeadObjectResponse.metadata() );
					final FileHandle fileHandle = new FileHandleImpl( FileSystemUtility.getPath( summary.key() ), FileSystemUtility.getName( summary.key() ), eHeadObjectResponse.contentLength(), createdDate, Date.from( summary.lastModified() ) );
					eResult.add( fileHandle );
				}
			}
			if ( isRecursively ) {
				for ( CommonPrefix prefix : eObjectListing.commonPrefixes() ) {
					eResult.addAll( getFileHandles( prefix.prefix(), isRecursively ) );
				}
			}
			eNextToken = eObjectListing.nextContinuationToken();
			eListObjectsRequest = ListObjectsV2Request.builder().bucket( getAmazonS3BucketName() ).continuationToken( eNextToken ).build();
			eObjectListing = getAmazonS3Client().listObjectsV2( eListObjectsRequest );
		}
		return eResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		LOGGER.log( Level.FINE, "Destroying the S3 file system..." );
		while ( !_threadCache.isEmpty() ) {
			LOGGER.info( "Waiting for destroying the S3 file system because some threads aren't finished yet." );
			try {
				Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
			}
			catch ( InterruptedException ignored ) {}
		}
		LOGGER.log( Level.FINE, "Destroyed the S3 file system." );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the created date from the provided Meta-Data.
	 * 
	 * @param aMetadata The Meta-Data from which to retrieve the created date.
	 * 
	 * @return TRhe created date.
	 */
	private Date getCreatedDate( Map<String, String> aMetadata ) {
		final String theCreatedDateString = aMetadata != null ? aMetadata.get( METADATA_CREATED_DATE ) : null;
		Date theCreatedDate = null;
		if ( theCreatedDateString != null ) {
			try {
				final Instant theInstant = Instant.from( DateFormat.NORM_DATE_FORMAT.getFormatter().parse( theCreatedDateString ) );
				theCreatedDate = new Date( theInstant.toEpochMilli() );
			}
			catch ( DateTimeException e ) {
				theCreatedDate = null;
			}
		}
		return theCreatedDate;
	}
}
