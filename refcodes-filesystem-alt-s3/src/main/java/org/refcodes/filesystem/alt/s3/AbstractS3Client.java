// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem.alt.s3;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.exception.Trap;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.profiles.ProfileFile;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.S3Object;

//@formatter:off
/**
 * Abstract class to be used for any S3 using service. An endpoint can be
 * specified (i.e. the location where Amazon SimpleDB will store the data):
 * 
 * Possible endpoints for SimpleDB can be retrieved from here:
 *
 * See "http://docs.amazonwebservices.com/general/latest/gr/rande.html#s3_region"
 * 
 *	<table summary="List of AWS Regions and Endpoints">
 * 		<tr>
 *			<td><strong>Region</strong></td><td><strong>Endpoint</strong></td><td><strong>Location Constraint</strong></td><td><strong>Protocol</strong></td>
 *		</tr>
 *		<tr>
 *			<td>US Standard *</td><td>s3.amazonaws.com</td><td>(none required)</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>US West (Oregon)</td><td>s3-us-west-2.amazonaws.com</td><td>us-west-2</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>US West (Northern California)</td><td>s3-us-west-1.amazonaws.com</td><td>us-west-1</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>EU (Ireland)</td><td>s3-eu-west-1.amazonaws.com</td><td>EU</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>Asia Pacific (Singapore)</td><td>s3-ap-southeast-1.amazonaws.com</td><td>ap-southeast-1</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>Asia Pacific (Tokyo)</td><td>s3-ap-northeast-1.amazonaws.com</td><td>ap-northeast-1</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>South America (Sao Paulo)</td><td>s3-sa-east-1.amazonaws.com</td><td>sa-east-1</td><td>HTTP and HTTPS</td>
 *		</tr>
 *	</table>
 */
//@formatter:on
abstract class AbstractS3Client {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( AbstractS3Client.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int THREAD_POOL_SIZE = 20;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static final String DEFAULT_REGION = "s3-eu-west-1.amazonaws.com";
	private S3Client _amazonS3Client = null;
	private String _amazonS3BucketName = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the S3 support by directly providing all needed information to
	 * setup the instance.
	 * 
	 * @param aBucketName The name of the bucket to use.
	 * @param aAccessKey The access key to use.
	 * @param aSecretKey The secret key to use.
	 */
	public AbstractS3Client( String aBucketName, String aAccessKey, String aSecretKey ) {
		this( aBucketName, aAccessKey, aSecretKey, DEFAULT_REGION );
	}

	//@formatter:off
	/**
	 * Constructs the S3 support by directly providing all needed information to
	 * setup the instance. An endpoint can be specified, i.e. the
	 * region where Amazon will store the data. 
	 * 
	 * Possible endpoints for SimpleDB can be retrieved  as stated above.
	 * 
	 * {@link http://docs.amazonwebservices.com/general/latest/gr/rande.html#s3_region}
	 * 
	 * @param aBucketName The name of the bucket to use.
	 * @param aAccessKey The access key to use.
	 * @param aSecretKey The secret key to use.
	 * @param aRegion The endpoint (Amazon region) to use.
	 */
	//@formatter:on
	public AbstractS3Client( String aBucketName, String aAccessKey, String aSecretKey, String aRegion ) {
		if ( aRegion == null ) {
			aRegion = DEFAULT_REGION;
		}
		_amazonS3Client = S3Client.builder().credentialsProvider( StaticCredentialsProvider.create( AwsBasicCredentials.create( aAccessKey, aSecretKey ) ) ).region( software.amazon.awssdk.regions.Region.of( aRegion ) ).build();
		_amazonS3BucketName = aBucketName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the bucket name to be used.
	 * 
	 * @return The bucket name.
	 */
	protected String getAmazonS3BucketName() {
		return _amazonS3BucketName;
	}

	/**
	 * Sets the bucket name to be used.
	 * 
	 * @param aAmazonS3BucketName The bucket name to be used.
	 */
	protected void setAmazonS3BucketName( String aAmazonS3BucketName ) {
		_amazonS3BucketName = aAmazonS3BucketName;
	}

	/**
	 * Retrieves the amazon S3 client to be used.
	 * 
	 * @return The S3 client to be used.
	 */
	protected S3Client getAmazonS3Client() {
		return _amazonS3Client;
	}

	/**
	 * Creates an S3 bucket.
	 * 
	 * @param aAmazonS3 The {@link S3Client} client.
	 * @param aBucketId The TID of the bucket to be created.
	 */
	public static void createBucket( S3Client aAmazonS3, String aBucketId ) {
		final CreateBucketRequest theCreateBucketRequest = CreateBucketRequest.builder().bucket( aBucketId ).build();
		aAmazonS3.createBucket( theCreateBucketRequest );
	}

	/**
	 * Clears (removes all content from) an S3 bucket.
	 * 
	 * @param aAmazonS3 The {@link S3Client} client.
	 * @param aBucketId The TID of the bucket to be cleared.
	 */
	protected static void clearBucket( final S3Client aAmazonS3, final String aBucketId ) {
		ListObjectsV2Request theListObjectsRequest = ListObjectsV2Request.builder().bucket( aBucketId ).build();
		ListObjectsV2Response theObjectListing = aAmazonS3.listObjectsV2( theListObjectsRequest );
		String eNextToken;
		while ( theObjectListing.contents() != null && !theObjectListing.contents().isEmpty() ) {
			final List<S3Object> theObjectSummaries = theObjectListing.contents();
			deleteS3Objects( aAmazonS3, aBucketId, theObjectSummaries );
			eNextToken = theObjectListing.nextContinuationToken();
			theListObjectsRequest = ListObjectsV2Request.builder().bucket( aBucketId ).continuationToken( eNextToken ).build();
			theObjectListing = aAmazonS3.listObjectsV2( theListObjectsRequest );
		}
	}

	/**
	 * Deletes an S3 bucket.
	 * 
	 * @param aAmazonS3 The {@link S3Client} client.
	 * @param aBucketId The TID of the bucket to be deleted.
	 */
	public static void deleteBucket( final S3Client aAmazonS3, final String aBucketId ) {
		clearBucket( aAmazonS3, aBucketId );
		final DeleteBucketRequest theDeleteBucketRequest = DeleteBucketRequest.builder().bucket( aBucketId ).build();
		aAmazonS3.deleteBucket( theDeleteBucketRequest );
	}

	/**
	 * Creates an {@link S3Client} "client".
	 * 
	 * @param aAccessKey The according access key for accessing amazon AWS.
	 * @param aSecretKey The secret access key for accessing amazon AWS.
	 * 
	 * @return The client represented by an {@link S3Client} instance.
	 */
	protected static S3Client createAmazonS3( String aAccessKey, String aSecretKey ) {
		return S3Client.builder().credentialsProvider( StaticCredentialsProvider.create( AwsBasicCredentials.create( aAccessKey, aSecretKey ) ) ).build();
	}

	/**
	 * Deletes the content described by the given {@link S3Object} list from an
	 * S3 bucket.
	 * 
	 * @param aAmazonS3 The {@link S3Client} client.
	 * @param aBucketId The TID of the bucket from which the objects are to be
	 *        deleted.
	 * @param aS3SummaryObjects The {@link S3Object} list describing the objects
	 *        to be deleted.
	 */
	protected static void deleteS3Objects( final S3Client aAmazonS3, final String aBucketId, List<S3Object> aS3SummaryObjects ) {
		final ExecutorService theExecutorService = Executors.newFixedThreadPool( THREAD_POOL_SIZE );
		final CountDownLatch theCountDownLatch = new CountDownLatch( aS3SummaryObjects.size() );
		for ( final S3Object eSummary : aS3SummaryObjects ) {
			theExecutorService.execute( new Runnable() {

				/**
				 * {@inheritDoc}
				 */
				@Override
				public void run() {
					try {
						final DeleteObjectRequest theDeleteObjectRequest = DeleteObjectRequest.builder().bucket( aBucketId ).key( eSummary.key() ).build();
						aAmazonS3.deleteObject( theDeleteObjectRequest );
					}
					catch ( Exception e ) {
						LOGGER.log( Level.WARNING, Trap.asMessage( e ), e );
					}
					finally {
						theCountDownLatch.countDown();
					}
				}
			} );
		}
		try {
			theCountDownLatch.await();
		}
		catch ( InterruptedException ignored ) {}
		theExecutorService.shutdown();
	}

	/**
	 * Retrieves an {@link S3Client} from a configuration file containing the
	 * access- and the secret key.
	 * 
	 * @param aConfigFile The configuration file used to configure the
	 *        {@link S3Client}.
	 * 
	 * @return An {@link S3Client}.
	 * 
	 * @throws IOException In case there were problems reading the configuration
	 *         file.
	 */
	protected static S3Client getAmazonS3Client( File aConfigFile ) throws IOException {
		final Properties theProperties = new Properties();
		theProperties.load( new FileInputStream( aConfigFile ) );
		return S3Client.builder().credentialsProvider( ProfileCredentialsProvider.builder().profileFile( ProfileFile.builder().content( aConfigFile.toPath() ).build() ).build() ).build();
	}
}
