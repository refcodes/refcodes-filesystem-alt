module org.refcodes.filesystem.alt.s3 {
	requires software.amazon.awssdk.services.s3;
	requires software.amazon.awssdk.auth;
	requires software.amazon.awssdk.profiles;
	requires software.amazon.awssdk.awscore;
	requires software.amazon.awssdk.core;
	requires software.amazon.awssdk.utils;
	requires software.amazon.awssdk.regions;
	requires software.amazon.awssdk.http;
	requires software.amazon.awssdk.identity.spi;
	requires org.refcodes.time;
	requires transitive org.refcodes.filesystem;
	requires java.logging;

	exports org.refcodes.filesystem.alt.s3;
}
