// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem.alt.s3;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.SleepLoopTime;
import org.refcodes.filesystem.ConcurrentAccessException;
import org.refcodes.filesystem.FileAlreadyExistsException;
import org.refcodes.filesystem.FileHandle;
import org.refcodes.filesystem.FileSystem;
import org.refcodes.filesystem.FileSystemUtility;
import org.refcodes.filesystem.IllegalFileException;
import org.refcodes.filesystem.IllegalKeyException;
import org.refcodes.filesystem.IllegalNameException;
import org.refcodes.filesystem.IllegalPathException;
import org.refcodes.filesystem.NoCreateAccessException;
import org.refcodes.filesystem.NoDeleteAccessException;
import org.refcodes.filesystem.NoListAccessException;
import org.refcodes.filesystem.NoReadAccessException;
import org.refcodes.filesystem.NoWriteAccessException;
import org.refcodes.filesystem.UnknownFileException;
import org.refcodes.filesystem.UnknownFileSystemException;
import org.refcodes.filesystem.UnknownKeyException;
import org.refcodes.filesystem.UnknownPathException;
import software.amazon.awssdk.services.s3.S3Client;

public class S3FileSystemTest {

	private static final String S3_BUCKET_NAME = S3FileSystemTest.class.getPackage().getName();
	private static final String S3_ACCESS_KEY = "ACCESSKEY";
	private static final String S3_SECRET_KEY = "SECRETKEY";
	private FileSystem _fileSystem;
	private S3Client _amazonS3;

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeEach
	public void beforeEach() throws IOException {
		if ( _amazonS3 == null ) {
			_amazonS3 = AbstractS3Client.createAmazonS3( S3_ACCESS_KEY, S3_SECRET_KEY );
		}
		// Create Bucket
		try {
			Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
		}
		catch ( InterruptedException ignored ) {}
		AbstractS3Client.createBucket( _amazonS3, S3_BUCKET_NAME );
		if ( _fileSystem == null ) {
			_fileSystem = new S3FileSystem( S3_BUCKET_NAME, S3_ACCESS_KEY, S3_SECRET_KEY );
		}
		try {
			Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
		}
		catch ( InterruptedException ignored ) {}
	}

	@AfterEach
	public void tearDown() {
		try {
			Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
		}
		catch ( InterruptedException ignored ) {}
		// Delete Bucket
		AbstractS3Client.deleteBucket( _amazonS3, S3_BUCKET_NAME );
		try {
			Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
		}
		catch ( InterruptedException ignored ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testHasFileString() throws IOException, FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IllegalKeyException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		final boolean exists = _fileSystem.hasFile( theFileHandle.toKey() );
		assertTrue( exists );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testHasFileStringString() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		createRecord( path, name, content );
		final boolean exists = _fileSystem.hasFile( path, name );
		assertTrue( exists );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testHasFile() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		final boolean exists = _fileSystem.hasFile( theFileHandle );
		assertTrue( exists );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testCreateFileString() throws FileAlreadyExistsException, NoCreateAccessException, IllegalKeyException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = _fileSystem.createFile( FileSystemUtility.toKey( path, name ) );
		assertNotNull( theFileHandle );
		final OutputStream out = _fileSystem.toFile( theFileHandle );
		out.write( content.getBytes() );
		out.flush();
		out.close();
		try {
			Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
		}
		catch ( InterruptedException ignored ) {}
		final boolean exists = _fileSystem.hasFile( theFileHandle );
		assertTrue( exists );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testCreateFileStringString() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		final boolean exists = _fileSystem.hasFile( theFileHandle );
		assertTrue( exists );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testGetFileString() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, IllegalKeyException, IllegalFileException, UnknownKeyException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		final FileHandle getFile = _fileSystem.getFileHandle( theFileHandle.toKey() );
		assertEquals( theFileHandle.getName(), getFile.getName() );
		assertEquals( theFileHandle.getPath(), getFile.getPath() );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testGetFileStringString() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, IllegalFileException, UnknownKeyException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		final FileHandle getFile = _fileSystem.getFileHandle( path, name );
		assertEquals( theFileHandle.getName(), getFile.getName() );
		assertEquals( theFileHandle.getPath(), getFile.getPath() );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testFromFileOutputStream() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoReadAccessException, ConcurrentAccessException, UnknownFileException, NoWriteAccessException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		_fileSystem.fromFile( theFileHandle, outputStream );
		assertEquals( content, outputStream.toString() );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testFromFile() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoReadAccessException, ConcurrentAccessException, UnknownFileException, NoWriteAccessException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		final InputStream inputStream = _fileSystem.fromFile( theFileHandle );
		final String readContent = new String( inputStream.readAllBytes() );
		assertEquals( content, readContent );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testToFileInputStream() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, NoReadAccessException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final String newContent = "Changed test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		assertNotNull( theFileHandle );
		final ByteArrayInputStream inputStream = new ByteArrayInputStream( newContent.getBytes() );
		_fileSystem.toFile( theFileHandle, inputStream );
		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		_fileSystem.fromFile( theFileHandle, outputStream );
		assertEquals( newContent, outputStream.toString() );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testToFile() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, NoReadAccessException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		final InputStream inputStream = _fileSystem.fromFile( theFileHandle );
		final String readContent = new String( inputStream.readAllBytes() );
		assertEquals( content, readContent );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testRenameFileString() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoDeleteAccessException, UnknownFileException, ConcurrentAccessException, NoWriteAccessException, NoReadAccessException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final String newName = "newfile.txt";
		FileHandle theFileHandle = createRecord( path, name, content );
		theFileHandle = _fileSystem.renameFile( theFileHandle, newName );
		assertEquals( newName, theFileHandle.getName() );
		assertEquals( path, theFileHandle.getPath() );
		final InputStream inputStream = _fileSystem.fromFile( theFileHandle );
		final String readContent = new String( inputStream.readAllBytes() );
		assertEquals( content, readContent );
		final boolean oldRecordExists = _fileSystem.hasFile( path, name );
		assertFalse( oldRecordExists );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testMoveFileString() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, NoDeleteAccessException, IllegalKeyException, NoReadAccessException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final String newName = "newfile.txt";
		FileHandle theFileHandle = createRecord( path, name, content );
		theFileHandle = _fileSystem.moveFile( theFileHandle, FileSystemUtility.toKey( path, newName ) );
		assertEquals( newName, theFileHandle.getName() );
		assertEquals( path, theFileHandle.getPath() );
		final InputStream inputStream = _fileSystem.fromFile( theFileHandle );
		final String readContent = new String( inputStream.readAllBytes() );
		assertEquals( content, readContent );
		final boolean oldRecordExists = _fileSystem.hasFile( path, name );
		assertFalse( oldRecordExists );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testDeleteFile() throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, NoDeleteAccessException, IllegalFileException {
		final String path = "/unit/test";
		final String name = "testfile.txt";
		final String content = "Test file";
		final FileHandle theFileHandle = createRecord( path, name, content );
		_fileSystem.deleteFile( theFileHandle );
		final boolean oldRecordExists = _fileSystem.hasFile( path, name );
		assertFalse( oldRecordExists );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testHasFilesStringBoolean() throws NoWriteAccessException, FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, ConcurrentAccessException, UnknownFileException, IOException, IllegalFileException {
		final String path1 = "/unit/test";
		final String name1 = "testfile1.txt";
		final String content1 = "Test file 1";
		createRecord( path1, name1, content1 );
		final String path2 = "/unit/test";
		final String name2 = "testfile2.txt";
		final String content2 = "Test file 2";
		createRecord( path2, name2, content2 );
		final String path3 = "/unit/test";
		final String name3 = "testfile3.txt";
		final String content3 = "Test file 3";
		createRecord( path3, name3, content3 );
		final String path4 = "/unit/test/sub";
		final String name4 = "testfile4.txt";
		final String content4 = "Test file 4";
		createRecord( path4, name4, content4 );
		boolean exists = _fileSystem.hasFiles( path1, true );
		assertTrue( exists );
		exists = _fileSystem.hasFiles( path1, false );
		assertTrue( exists );
		exists = _fileSystem.hasFiles( path4, true );
		assertTrue( exists );
		exists = _fileSystem.hasFiles( path4, false );
		assertTrue( exists );
		exists = _fileSystem.hasFiles( path4 + "filler", true );
		assertFalse( exists );
		exists = _fileSystem.hasFiles( path4 + "filler", false );
		assertFalse( exists );
	}

	@Test
	@Disabled("Deactivate because of no Amazon S3 account at hand!")
	public void testGetFilesStringBoolean() throws NoWriteAccessException, FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, ConcurrentAccessException, UnknownFileException, IOException, UnknownPathException, IllegalFileException {
		final String path1 = "/unit/test";
		final String name1 = "testfile1.txt";
		final String content1 = "Test file 1";
		final FileHandle theFileHandle1 = createRecord( path1, name1, content1 );
		final String path2 = "/unit/test";
		final String name2 = "testfile2.txt";
		final String content2 = "Test file 2";
		final FileHandle theFileHandle2 = createRecord( path2, name2, content2 );
		final String path3 = "/unit/test";
		final String name3 = "testfile3.txt";
		final String content3 = "Test file 3";
		final FileHandle theFileHandle3 = createRecord( path3, name3, content3 );
		final String path4 = "/unit/test/sub";
		final String name4 = "testfile4.txt";
		final String content4 = "Test file 4";
		final FileHandle theFileHandle4 = createRecord( path4, name4, content4 );
		List<FileHandle> theFileHandles = _fileSystem.getFileHandles( path1, false );
		assertEquals( 3, theFileHandles.size() );
		theFileHandles = _fileSystem.getFileHandles( path4, false );
		assertEquals( 1, theFileHandles.size() );
		theFileHandles = _fileSystem.getFileHandles( path1, true );
		assertEquals( 4, theFileHandles.size() );
		for ( FileHandle theFileHandle : theFileHandles ) {
			if ( !( ( theFileHandle.getName().equals( theFileHandle1.getName() ) || theFileHandle.getName().equals( theFileHandle2.getName() ) || theFileHandle.getName().equals( theFileHandle3.getName() ) || theFileHandle.getName().equals( theFileHandle4.getName() ) ) && ( theFileHandle.getPath().equals( theFileHandle1.getPath() ) || theFileHandle.getPath().equals( theFileHandle2.getPath() ) || theFileHandle.getPath().equals( theFileHandle3.getPath() ) || theFileHandle.getPath().equals( theFileHandle4.getPath() ) ) ) ) {
				fail( "Should not reach this code!" );
			}
		}
	}

	private FileHandle createRecord( String aPath, String aName, String aContent ) throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, NoListAccessException, IOException, NoWriteAccessException, ConcurrentAccessException, UnknownFileException, IllegalFileException {
		final FileHandle theFileHandle = _fileSystem.createFile( aPath, aName );
		assertNotNull( theFileHandle );
		final OutputStream outputStream = _fileSystem.toFile( theFileHandle );
		outputStream.write( aContent.getBytes() );
		outputStream.flush();
		outputStream.close();
		try {
			Thread.sleep( SleepLoopTime.NORM.getTimeMillis() );
		}
		catch ( InterruptedException ignored ) {}
		return theFileHandle;
	}
}
